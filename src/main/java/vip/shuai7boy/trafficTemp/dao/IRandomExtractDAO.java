package vip.shuai7boy.trafficTemp.dao;


import vip.shuai7boy.trafficTemp.domain.RandomExtractCar;
import vip.shuai7boy.trafficTemp.domain.RandomExtractMonitorDetail;

import java.util.List;

/**
 * 随机抽取car信息管理DAO类
 *
 * @author root
 */
public interface IRandomExtractDAO {
    void insertBatchRandomExtractCar(List<RandomExtractCar> carRandomExtracts);

    void insertBatchRandomExtractDetails(List<RandomExtractMonitorDetail> r);
}
