package vip.shuai7boy.trafficTemp.dao;

import vip.shuai7boy.trafficTemp.domain.CarTrack;

import java.util.List;

public interface ICarTrackDAO {

    /**
     * 批量插入车辆轨迹信息
     * @param carTracks
     */
    void insertBatchCarTrack(List<CarTrack> carTracks);
}
