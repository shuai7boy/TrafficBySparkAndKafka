package vip.shuai7boy.trafficTemp.dao;

import vip.shuai7boy.trafficTemp.domain.Task;

/**
 * 任务管理 DAO接口
 */
public interface ITaskDAO {

    /**
     * 根据Task主键查询指定任务
     * @param taskId
     * @return
     */
    Task findTaskById(long taskId);
}
