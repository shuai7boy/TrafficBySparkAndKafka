package vip.shuai7boy.trafficTemp.dao;

import vip.shuai7boy.trafficTemp.domain.Area;

import java.util.List;

public interface IAreaDao {
    List<Area> findAreaInfo();
}
