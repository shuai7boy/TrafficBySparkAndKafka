package vip.shuai7boy.trafficTemp.dao.impl;

import vip.shuai7boy.trafficTemp.constant.Constants;
import vip.shuai7boy.trafficTemp.dao.IWithTheCarDAO;
import vip.shuai7boy.trafficTemp.jdbc.JDBCHelper;
import vip.shuai7boy.trafficTemp.util.DateUtils;

public class WithTheCarDAOImpl implements IWithTheCarDAO {
    @Override
    public void updateTestData(String cars) {
        JDBCHelper jdbcHelper = JDBCHelper.getInstance();
        String sql = "UPDATE task set task_param = ? WHERE task_id = 3";
        Object[] params = new Object[]{"{\"startDate\":[\"" + DateUtils.getTodayDate() + "\"],\"endDate\":[\"" + DateUtils.getTodayDate() + "\"],\"" + Constants.FIELD_CARS + "\":[\"" + cars + "\"]}"};
        jdbcHelper.executeUpdate(sql, params);
    }
}
