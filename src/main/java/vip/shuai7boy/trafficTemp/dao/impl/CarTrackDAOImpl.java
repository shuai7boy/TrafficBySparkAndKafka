package vip.shuai7boy.trafficTemp.dao.impl;

import vip.shuai7boy.trafficTemp.dao.ICarTrackDAO;
import vip.shuai7boy.trafficTemp.domain.CarTrack;
import vip.shuai7boy.trafficTemp.jdbc.JDBCHelper;

import java.util.ArrayList;
import java.util.List;

public class CarTrackDAOImpl implements ICarTrackDAO {
    @Override
    public void insertBatchCarTrack(List<CarTrack> carTracks) {
        JDBCHelper jdbcHelper = JDBCHelper.getInstance();
        String sql = "INSERT INTO car_track VALUES(?,?,?,?)";
        List<Object[]> params = new ArrayList<>();
        for (CarTrack c : carTracks) {
            /**
             * 添加到车辆轨迹表中
             */
            params.add(new Object[]{c.getTaskId(), c.getDate(), c.getCar(), c.getTrack()});

        }
        jdbcHelper.executeBatch(sql, params);
    }
}
