package vip.shuai7boy.trafficTemp.dao.impl;

import vip.shuai7boy.trafficTemp.dao.IAreaDao;
import vip.shuai7boy.trafficTemp.domain.Area;
import vip.shuai7boy.trafficTemp.jdbc.JDBCHelper;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class AreaDaoImpl implements IAreaDao {

    @Override
    public List<Area> findAreaInfo() {
        final List<Area> areas = new ArrayList<>();
        final String sql = "SELECT * FROM area_info";

        JDBCHelper jdbcHelper = JDBCHelper.getInstance();
        
        jdbcHelper.executeQuery(sql, null, new JDBCHelper.QueryCallback() {
            @Override
            public void process(ResultSet rs) throws Exception {
                if (rs.next()) {
                    String areaId = rs.getString(1);
                    String areaName = rs.getString(2);
                    areas.add(new Area(areaId, areaName));
                }
            }
        });
        return areas;
    }
}
