package vip.shuai7boy.trafficTemp.dao.factory;

import vip.shuai7boy.trafficTemp.dao.*;
import vip.shuai7boy.trafficTemp.dao.impl.*;

public class DAOFactory {

    public static ITaskDAO getTaskDAO() {
        return new TaskDAOImpl();
    }
    public static IMonitorDAO getMonitorDAO(){
        return new MonitorDAOImpl();
    }

    public static IRandomExtractDAO getRandomExtractDAO(){
        return new RandomExtractDAOImpl();
    }

    public static ICarTrackDAO getCarTrackDAO(){
        return new CarTrackDAOImpl();
    }

    public static IWithTheCarDAO getWithTheCarDAO(){
        return new WithTheCarDAOImpl();
    }

    public static IAreaDao getAreaDao() {
        return  new AreaDaoImpl();

    }


}
