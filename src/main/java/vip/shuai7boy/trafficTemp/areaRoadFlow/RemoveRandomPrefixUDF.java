package vip.shuai7boy.trafficTemp.areaRoadFlow;

import org.apache.spark.sql.api.java.UDF1;

/**
 * in:：0_朝阳区：来广营街道
 * return:朝阳区：来广营街道
 */
public class RemoveRandomPrefixUDF implements UDF1<String, String> {

    @Override
    public String call(String val) throws Exception {
        return val.split("_")[1];
    }
}
