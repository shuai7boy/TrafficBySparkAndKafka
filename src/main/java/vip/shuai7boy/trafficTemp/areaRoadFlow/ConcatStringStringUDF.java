package vip.shuai7boy.trafficTemp.areaRoadFlow;

import org.apache.spark.sql.api.java.UDF3;

/**
 * 将两个字段拼接起来（使用指定分隔符）
 * 海淀区：建材西路
 */
public class ConcatStringStringUDF implements UDF3<String, String, String, String> {
    private static final long serialVersionUID = 1L; //serialVersionUID序列化时进行校验版本是否一致

    @Override
    public String call(String area_name, String road_id, String split) throws Exception {
        return area_name + split + road_id;
    }
}
