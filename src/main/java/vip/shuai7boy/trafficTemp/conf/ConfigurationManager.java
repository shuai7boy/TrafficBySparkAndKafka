package vip.shuai7boy.trafficTemp.conf;

import java.io.InputStream;
import java.util.Properties;

/***
 * 配置管理组件
 * 在第一次加载的时候就加载Properties
 */
public class ConfigurationManager {

    //设置成私有的，避免外界使用ConfigurationManager直接调用，不小心更新该值
    //从而导致整个程序状态错误，乃至崩溃
    private static Properties prop = new Properties();

    /***
     * 静态代码块
     * 将获取配置文件代码放在这个里面，这样做的好处是，在JMV首次读取此类的时候就会加载，而且以后使用的话不会再重复加载，提高效率。
     */
    static {
        try {
            InputStream in = ConfigurationManager.class.getClassLoader().getResourceAsStream("my.properties");
            prop.load(in);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getProperty(String key) {
        return prop.getProperty(key);
    }

    //获取整数配置项
    public static Integer getInteger(String key) {
        String str = prop.getProperty(key);
        try {
            return Integer.valueOf(str);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    //获取long类型配置项
    public static Long getLong(String key) {
        String str = prop.getProperty(key);
        try {
            return Long.valueOf(str);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
        return 0L;
    }

    //获取bool配置项
    public static Boolean getBoolean(String key) {
        String str = prop.getProperty(key);
        try {
            return Boolean.valueOf(str);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

}









