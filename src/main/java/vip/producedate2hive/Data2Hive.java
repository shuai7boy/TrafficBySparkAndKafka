package vip.producedate2hive;

//1.代码方式创建hive表并添加数据

import org.apache.spark.sql.SparkSession;

import java.io.File;

public class Data2Hive {

    /**
     * 下面代码是创建在本地，仅供参考
     * 具体在服务器，我们需要手动创建
     * @param args
     */
    public static void main(String[] args) {


        String warehouseLocation  = new File("spark-warehouse").getAbsolutePath();
        SparkSession spark = SparkSession.builder()
                .appName("traffic2hive")
                .enableHiveSupport()
                .getOrCreate();

        spark.sql("USE traffic");
        spark.sql("DROP TABLE IF EXISTS monitor_flow_action");
        //在hive中创建monitor_flow_action表
        spark.sql("CREATE TABLE IF NOT EXISTS monitor_flow_action "
                + "(date STRING,monitor_id STRING,camera_id STRING,car STRING,action_time STRING,speed STRING,road_id STRING,area_id STRING) "
                + "row format delimited fields terminated by '\t' ");
        spark.sql("load data local inpath '/data/spark/monitor_flow_action' into table monitor_flow_action");

        //在hive中创建monitor_camera_info表
        spark.sql("DROP TABLE IF EXISTS monitor_camera_info");
        spark.sql("CREATE TABLE IF NOT EXISTS monitor_camera_info (monitor_id STRING, camera_id STRING) row format delimited fields terminated by '\t'");
        spark.sql("LOAD DATA "
                + "LOCAL INPATH '/data/spark/monitor_camera_info'"
                + "INTO TABLE monitor_camera_info");

        System.out.println("========data2hive finish========");
        spark.stop();
    }
}
